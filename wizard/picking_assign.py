from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError


class PickingAssign(models.TransientModel):
    _name = 'assign'

    @api.multi
    def multiple_assign(self):
        self.ensure_one()
        picking_ids = self._context.get('active_ids')
        pickings = self.env['stock.picking'].browse(picking_ids)
        for pick in pickings:
            if pick.picking_type_id.code != 'outgoing':
                raise UserError(_('All selected piking should be Outgoing Type'))

        for pick in pickings:
            if pick.state in ['partially_available', 'assigned', 'done']:
                raise UserError(_('All selected piking should be Waiting Availability'))

        for pick in pickings:
            picking = self.env['stock.picking'].search([('id', '=', pick.id)])
            picking.action_assign()

        return True
