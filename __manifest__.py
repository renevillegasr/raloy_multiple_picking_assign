{
    'name': 'Raloy Multiple Picking Assign',
    'version': '1.0.0.0',
    'summary': 'This app helps to do the assigned process over multiple stock picks',
    'author': 'René Villegas',
    'category': 'Warehouse',
    'description': """Generate stock assignment to multiple stock pickings""",
    'depends': ['sale', 'stock'],
    'data': [
        'wizard/picking_assign_wizard.xml'
    ],
    'installable': True,
    'auto_install': False,
}